package id.ub.tugas6_195150400111029;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PerpustakaanService {
    @GET("buku")
    Call<List<Buku>> listBuku();

    @GET("buku/{id}")
    Call<id.ub.restapi_195150400111029.Buku> getBuku(@Path("id") String id);

}
